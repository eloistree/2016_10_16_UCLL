﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour {


    public GameObject _theCube;


    /**
     That control the speed of the cube and it deal with 1 scare by second.
         */

    [Tooltip("That control the speed of the cube and it deal with 1 scare by second.")]
    [SerializeField]
    private float _speed = 1;


    [Header("Information personel")]
    [SerializeField]
    private string _userName;
    [SerializeField]
    private string _userBirthday;

    


    void Update () {


        _theCube.transform.position += _theCube.transform.forward * Time.deltaTime *_speed;
	}
}
